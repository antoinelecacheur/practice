import React from 'react'
import { Link } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const GenericItem = ({ url, name, icon, className, toggleEntry, open }) => (
    <ListItem button className={`link ${className}`} component={Link} to={url} onClick={toggleEntry ? () => toggleEntry() : () => { }}>
        <ListItemIcon>
            {icon}
        </ListItemIcon>
        <ListItemText primary={name} />
        {toggleEntry ? open ? <ExpandLess /> : <ExpandMore /> : <></>}
    </ListItem >
)

export default GenericItem