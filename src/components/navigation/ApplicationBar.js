import React from 'react'
import { Toolbar, IconButton, Typography, AppBar, Grid } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import { formatageDate } from './../../utils/date';

const ApplicationBar = ({ barClass, buttonClass, handleDrawerOpen, text }) => {

    return (
        <AppBar position="fixed" className={barClass}        >
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={buttonClass}
                >
                    <MenuIcon />
                </IconButton>
                <Grid container>
                    <Grid item xs={10}>

                        <Typography variant="h5" gutterBottom={true} color="textPrimary">
                            <Link to='/' className='link'>
                                ATHENES : Analyse du Traitement Homogène des Etudes en régioN et de consEil en expertiSe
                            </Link>
                        </Typography>
                    </Grid>
                    <Grid item xs={2}>
                        <Typography variant="h6" gutterbottom="true" color="textPrimary">{formatageDate(Date.now())}</Typography>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}

export default ApplicationBar