import React from 'react'
import { Divider, List, Collapse } from '@material-ui/core'
import GenericItem from './GenericItem'

const GenericList = ({ liste, classes, etat, toggleEntry }) => {

    return liste !== undefined ?
        <List component="nav">
            {liste.map(listItem => {
                if (listItem.divider) {
                    return <Divider />
                } else if (listItem.nestedItems) {
                    return (<><GenericItem name={listItem.name} icon={listItem.icon} toggleEntry={() => toggleEntry(listItem.entry)} open={etat[listItem.entry]} />
                        <Collapse in={etat[listItem.entry]} timeout="auto" unmountOnExit>
                            {
                                listItem.nestedItems.map(item => {
                                    if (item.nestedItems) {
                                        return (<><GenericItem name={item.name} icon={item.icon} toggleEntry={() => toggleEntry(item.entry)} open={etat[item.entry]} className={classes.nested} />
                                            <Collapse in={etat[item.entry]} timeout="auto" unmountOnExit>
                                                {
                                                    item.nestedItems.map(subItem => {
                                                        return <GenericItem name={subItem.name} url={subItem.url} icon={subItem.icon} className={classes.nested2} />
                                                    })
                                                }</Collapse></>)
                                    } else {
                                        return <GenericItem name={item.name} url={item.url} icon={item.icon} className={classes.nested} />
                                    }
                                })}</Collapse></>)
                } else {
                    return <GenericItem name={listItem.name} url={listItem.url} icon={listItem.icon} />
                }
            })}

        </List>
        : <></>
}

export default GenericList