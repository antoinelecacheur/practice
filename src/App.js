import React from 'react';
import './App.css';
import MiniDrawer from './components/navigation/Drawer';
import { BrowserRouter } from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
      <MiniDrawer >
        Test
      </MiniDrawer>
    </BrowserRouter>
  )
}

export default App;
