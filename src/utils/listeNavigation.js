import React from 'react'
import Home from '@material-ui/icons/Home'
import SupervisorAccount from '@material-ui/icons/SupervisorAccount'
import ListAlt from '@material-ui/icons/ListAlt'
import Business from '@material-ui/icons/Business'
import RateReview from '@material-ui/icons/RateReview'
import NewReleases from '@material-ui/icons/NewReleases'
import Note from '@material-ui/icons/Note'
import Create from '@material-ui/icons/Create'
import EventNote from '@material-ui/icons/EventNote'
import Slideshow from '@material-ui/icons/Slideshow'
import CreateNewFolder from '@material-ui/icons/CreateNewFolder'
import Poll from '@material-ui/icons/Poll'
import Dashboard from '@material-ui/icons/Dashboard'
import ChromeReaderMode from '@material-ui/icons/ChromeReaderMode'
import Search from '@material-ui/icons/Search'

const listeNavigation = [
    { name: 'Accueil', url: '/', icon: <Home color='primary' /> },
    {
        name: 'Administration', icon: <SupervisorAccount color='primary' />, entry: 'administrationOpen',
        nestedItems: [
            { name: 'Droits', url: '/droits', icon: <SupervisorAccount color='primary' /> },
            {
                name: 'Référentiels', icon: <ListAlt color='primary' />, entry: 'referentielsOpen',
                nestedItems: [
                    { name: 'Partenaires', url: '/administrationPartenaires', icon: <Business color='primary' /> }
                ]
            },
            { name: 'Processus de lecture', url: '/valorisation', icon: <RateReview color='primary' /> },
            { name: "Actualités nationales du réseau de l'action régionale", url: '/actualites', icon: <NewReleases color='primary' /> }
        ]
    },
    { divider: true },
    {
        name: 'Création / Modification', icon: <Note color='primary' />, entry: 'creationOpen',
        nestedItems: [
            { name: 'Études', url: '/etude', icon: <Create color='primary' /> },
            { name: 'Études avec partenaire', url: '/etudePartenaire', icon: <EventNote color='primary' /> },
            { name: 'Actions de conseil-expertise', url: '/actionsCE', icon: <Slideshow color='primary' /> },
            { name: 'Rencontres de partenaires', url: '/rencontrePartenaire', icon: <CreateNewFolder color='primary' /> },
            { name: 'Produits sur mesure', url: '/psm', icon: <Poll color='primary' /> }
        ]
    },
    {
        name: 'Restitutions', icon: <Dashboard color='primary' />, entry: 'suiviOpen',
        nestedItems: [
            { name: 'Études', url: '/restitution-publications', icon: <ChromeReaderMode color='primary' /> },
            { name: 'Actions de conseil-expertise', url: '/restitution-ce', icon: <ChromeReaderMode color='primary' /> },
        ]
    },
    { divider: true },
    { name: 'Recherche', url: '/recherche', icon: <Search color='primary' /> }
]

export default listeNavigation